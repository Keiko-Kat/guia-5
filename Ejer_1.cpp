#include "Nodo.h"
#include "Arbol.h"
#include "Ejer_1.h"
#include <string>
#include <fstream>
#include <iostream>
using namespace std;




void lectura_pre(Nodo *raiz){
	//lectura de preorden
	if(raiz != NULL){
		if(raiz->numero < 0){
			//si el numero es negativo se imprime entre parentesis
			cout<<"-("<<raiz->numero<<")-";
		}else{
			//si es positivo o cero se imprime como es
			cout<<"-"<<raiz->numero<<"-";
		}
		//se hace la llamada recursiva hacia la izquierda primero
		lectura_pre(raiz->izq);
		//la segunda llamada recursiva es hacia la derecha
		lectura_pre(raiz->der);
	}
}

void lectura_in(Nodo *raiz){
	//lectura inorden
	if(raiz != NULL){
		//se hace la llamada hacia a la izquierda
		lectura_in(raiz->izq);
		if(raiz->numero < 0){
			//si el numero es negativo se imprime entre parentesis
			cout<<"-("<<raiz->numero<<")-";
		}else{
			//si es positivo o cero se imprime como es
			cout<<"-"<<raiz->numero<<"-";
		}
		//la segunda llamada recursiva es hacia la derecha		
		lectura_in(raiz->der);
	}
}

void lectura_pos(Nodo *raiz){
	//lectura posorden
	if(raiz != NULL){
		//la primera llamada recursiva es hacia la izquierda
		lectura_pos(raiz->izq);
		//la segunda llamada recursiva es hacia la derecha
		lectura_pos(raiz->der);
		if(raiz->numero < 0){
			//si el numero es negativo se imprime entre parentesis
			cout<<"-("<<raiz->numero<<")-";
		}else{
			//si es positivo o cero se imprime como es
			cout<<"-"<<raiz->numero<<"-";
		}
	}
}





int main (void) {
	//se crea una instancia de "Ejercicio" para manejar el arbol
    Ejercicio e = Ejercicio();
    //y el arbol se instancia dentro de "ejercicio"
    Arbol *arbol = e.get_arbol();
    //se instancian in, para ingresar strings desde el teclado, 
    //los bool is_true y bul cumplen la funcion de validar el funcionamiento de las funciones 
    string in;
    bool is_true, bul;
    //se instancia el primer nodo como una variable vacia y tres int
    //num es el numero ingresado para instanciar los nodos, temp lleva las ordenes del menu 
    //y remp se utiliza en el reemplazo de nodos en el arbol
    Nodo *nodo;
    int num, temp = 1, remp;
    //se abre un while para generar un ciclo en el que se ejecuta la instanciacion del primer nodo
    while(temp != 0){
		//se pide un ingreso desde teclado para inicializar el arbol
		cout<<"Ingrese un numero entero para iniciar el arbol:"<<endl;
		cout<<"   ~~~> ";
		//se utiliza getline para leer la entrada del teclado como un string
		getline(cin, in);
		//se utiliza una funcion de "Ejercicio" para validar que el ingresos sea un numero
		is_true = e.validar_int(in);
		//is_true dice si el ingreso es un numero o si posee caracteres extraños
		if(is_true == true){
			//si retorna verdadero el string ingresado se transforma a un int
			num = stoi(in);
			//se genera el primer nodo del arbol
			arbol->crear(num);
			//y se setea el temp a 0 para salir del while
			temp = 0;
		}else{
			//si se realiza algun ingreso no numeral, se pide un nuevo intento
			cout<<"Ingreso no válido"<<endl;
			cout<<"Intente nuevamente"<<endl;
		}
	}
	//se llama a la funcion menu de "Ejercicio" para generar una opcion valida de funcionamiento
    temp = e.menu();
    //se instancia el nodo raiz como la raiz del arbol de "Ejercicio"
    Nodo *raiz = arbol->get_raiz();
    
    //se abre un while para generar un ciclo infinito para ejecutar el programa
    while(temp != 0){
		//el valor default de bul es false, por lo que se resetea al inicio de cada 
		bul = false;
		//se abre un switch dependiente del temp ingresado en menu
		switch(temp){
			//el caso "1" es ingreso, por lo que se pide ingresar un numero entero al arbol
			case 1: cout<<"Ingrese un numero entero:"<<endl;
					cout<<"   ~~~> ";
					getline(cin, in);
					//se obtiene un string del teclado y se hace la validacion de int
					is_true = e.validar_int(in);
					//si is_true es verdadero, se transforma el string a int y se ingresa a la funcion insertar perteneciente al arbol
					if(is_true == true){
						num = stoi(in);
						//la funcion pide un numero, ingresado por teclado, el booleano bul y la raiz del arbol, los dos ultimos se pasan por referencia
						arbol->insertar_balanceo(num, raiz, bul);
					}else{
						//si is_true es falso se da un mensaje de error y se corta el switch
						cout<<"Ingreso no válido"<<endl;
					}
					
					break;
			
			//el caso "2" es eliminacion, por lo que se pregunta que numero se desea eliminar
			case 2: cout<<"Ingrese el numero entero a eliminar:"<<endl;
					cout<<"   ~~~> ";
					//se obtiene un string del teclado y se hace la validacion de int					
					getline(cin, in);
					is_true = e.validar_int(in);
					//si is_true es verdadero, se transforma el string a int y se ingresa a la funcion eliminar perteneciente al arbol
					if(is_true == true){
						num = stoi(in);
						//la funcion pide un numero, ingresado por teclado, el booleano bul y la raiz del arbol, los dos ultimos se pasan por referencia
						arbol->eliminar_balanceo(num, raiz, bul);
					}else{
						//si is_true es falso se da un mensaje de error y se corta el switch
						cout<<"Ingreso no válido"<<endl;
					}					
					break;
			
			
			//el caso "3" es modificacion, por lo que se pregunta que numero se desea reemplazar
			case 3: cout<<"Ingrese el numero entero a modificar:"<<endl;
					cout<<"   ~~~> ";
					//se hacen las validaciones para la transformacion del string a int
					getline(cin, in);
					is_true = e.validar_int(in);
					//si is_true es verdadero se hace la transformacion a int y se pide un entero para reemplazarlo
					if(is_true == true){
						num = stoi(in);
						cout<<"Ingrese el numero entero que lo reemplace:"<<endl;
						cout<<"   ~~~> ";
						//se hacen las validaciones para la transformacion del string a int
						getline(cin, in);
						is_true = e.validar_int(in);
						if(is_true == true){
							//si is_true es verdadero se hace la transformacion a int
							remp = stoi(in);
							//la funcion pide two numeros, ingresado por teclado, el booleano bul y la raiz del arbol, los dos ultimos se pasan por referencia
							arbol->modifica_balanceo(num, remp, raiz, bul);
						}else{
							//si is_true es falso se da un mensaje de error y se corta el switch
							cout<<"Ingreso no válido"<<endl;
						}	
					}else{
						//si is_true es falso se da un mensaje de error y se corta el switch
						cout<<"Ingreso no válido"<<endl;
					}				
					

					
					break;
			
			//el caso "4" es la lectura en pre-orden, la funcion pide la raiz del arbol
			case 4: lectura_pre(raiz);
					cout<<endl;
					break;
			
			//el caso "5" es la lectura en in-orden, la funcion pide la raiz del arbol
			case 5: lectura_in(raiz);
					cout<<endl;
					break;
			
			//el caso "6" es la lectura en pos-orden, la funcion pide la raiz del arbol
			case 6: lectura_pos(raiz);
					cout<<endl;
					break;
			
			//el caso "7" es la creacion del grafo, la funcion pide la raiz derl arbol
			case 7:	arbol->crearGrafo(raiz);
					break;
			
			default: break;
		}	
		//se llama a la funcion menu de "Ejercicio" nuevamente para permitir el loop virtualmente infinito de la funcion			
		temp = e.menu();
		
		
	}
	//si se ingresa 0 en el menu, el programa se acaba	
    return 0;
}
