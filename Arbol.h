#include <iostream>
#include <string>
#include <fstream>
#include "Nodo.h"
using namespace std;

#ifndef ARBOL_H
#define ARBOL_H

class Arbol {
    public:
        Nodo *raiz = NULL;
        /* constructor*/
        Arbol();
        /* crea un nuevo nodo, recibe una instancia de numeros. */
        void crear (int numero);
        Nodo *crear_nodo(int numero);
        //retorna un nodo raiz
        Nodo *get_raiz();
        //recibe un numero, un nodo y un booleano, e ingresa un nodo de valor num
        void insertar_balanceo(int num, Nodo *&raiz, bool &bul);
        //recibe un numero, un nodo y un booleano, y elimina el nodo de valor num
        void eliminar_balanceo(int num, Nodo *&raiz, bool &bul);
        //recibe dos numeros, un nodo y un booleano, y reemplaza num, por reemplazo
        void modifica_balanceo(int num, int reemplazo, Nodo *&raiz, bool &bul);
        
        //reciben un nodo y un booleano y reorganizan el arbol a partir del nod entregado
        void reestructura_izq(Nodo *&raiz, bool &bul);
        void reestructura_der(Nodo *&raiz, bool &bul);
        
        //recibe un nodo, un archivo y un contador, llenan el archivo ingresado con los hijos del nodo ingresado
        int recorrerArbol(Nodo *p, ofstream &, int cont);
        //crea un archivo que guarda la informacion del arbol y la imprime en forma de un grafo
        void crearGrafo(Nodo *raiz);
};
#endif
