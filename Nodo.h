#include <iostream>
#include <string>
#include <fstream>
#ifndef NODO_H
#define NODO_H

/* define la estructura del nodo. */
typedef struct _Nodo {
    int numero;
    std::string info;
    std::string label;
    struct _Nodo *izq;
    struct _Nodo *der;
} Nodo;

#endif
