#include "Nodo.h"
#include "Arbol.h"
using namespace std;
#include <string>
#include <iostream>
#include <fstream>

class Ejercicio {
    private:
    //se define la caracteristica arbol, del tipo "Arbol"
        Arbol *arbol = NULL;

    public:
        /* constructor */
        Ejercicio() {
			//se instancia arbol como un objeto tipo "Arbol"
            this->arbol = new Arbol();
        }
        
        Arbol *get_arbol() {
			//retorna los contenidos de arbol
            return this->arbol;
        }
        
		bool validar_int(string in){
			//para validar la entrada de un int primero se define is_true como verdadero por default
			bool is_true = true;
			
			for(int j = 0; j < in.size(); j++){
				// se comparan los valores ASCII
				if((in[j] < 48 || in[j] >57) && in[j] != 45){
					//si el contenido del string no coincide con un valor ASCII numerico o de "-" is_true se iguala a falso
					is_true = false;
				}
			}
			//se retorna un valor de is_true que depende de la naturaleza del string ingresado
			return is_true;
		}
		
        int menu(){
			//esta funcion define un menú de opciones para el funcionamiento del programa
			//se definen un string in, para el ingreso de teclado, un bool y dos int
			string in;
			bool is_true;
			//is_true define si el ingreso del teclado es un numero valido
			//temp es la opcion que saldra del menu, y cond mantiene el menu corriendo en el caso que no se ingrese un valor adecuado
			int temp, cond = 0;
			while(cond == 0){
				//primero se imprimen las opciones del menu, dando al usuario control sobre que hacer luego
				cout<<"Agregar otro número [1]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Eliminar número     [2]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Modificar número    [3]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer preorden:      [4]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer inorden:       [5]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Leer posorden:      [6]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Generar grafo:      [7]"<<endl;
				cout<<"~~~~~~~~~~~~~~~~~~~~~~~"<<endl;
				cout<<"Salir               [0]"<<endl;
				cout<<"-----------------------"<<endl;
				cout<<"-----------------------"<<endl;
				cout<<"Opción:  ";
				getline(cin, in);
				//in recibe un ingreso del teclado que se pasa a validar si puede ser un int valido
				is_true = this->validar_int(in);
				if(is_true == true){
					//si is_true es verdadero in se pasa a temp como un int
					temp = stoi(in);
					//y luego se comprueba que sea una opcion valida
					if(temp < 0 || temp > 7){
						//si el ingreso no se encuentra entre 0-7, se envia un mensaje de error
						//ya que no ha habido salida valida, cond no se modifica y se devuelve al inicio del ciclo
						cout<< "opción no valida, intente nuevamente"<<endl;
					}else{
						//si el ingreso es valido cond se iguala a 1, acabando el ciclo
						cond = 1;
					}
				}else{
					//si is_true es falso, se envia un mensaje de error
					//ya que no ha habido un ingreso valido, cond no se modifica y se devuelve al inicio del ciclo
					cout<<"Ingreso no válido, intente nuevamente"<<endl;
				}				
				
			}
			//se retorna temp, como un dato que indica que opcion decidio usar el usuario
			return temp;				
		}		
		
		
};
