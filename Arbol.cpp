#include <iostream>
#include <string>
#include <fstream>
using namespace std;

#include "Nodo.h"
#include "Arbol.h"

Arbol::Arbol() {}

void Arbol::crear(int numero){
	//esta funcion crea un nodo y lo ubica como raiz
	Nodo *tmp;
	//se crea el nodo
	tmp = new Nodo;
	
	//se inicializan las variables internas
	//con los valores base o el ingresado por el usuario
	tmp->numero = numero;
	tmp->info = to_string(numero);
	tmp->label = "0";
	tmp->izq = NULL;
	tmp->der = NULL;
	//se ubica el nodo en posicion raiz
	this->raiz = tmp;
}

Nodo *Arbol::crear_nodo(int numero){
	Nodo *tmp;
	/*se crea el nodo*/
	tmp = new Nodo;
	
	//se inicializan las variables internas
	//con los valores base o el ingresado por el usuario
	
	tmp->numero = numero;
	tmp->info = to_string(numero);
	tmp->label = "0";
	tmp->izq = NULL;
	tmp->der = NULL;
	//se retorna el nodo
	return tmp;
}
	
Nodo *Arbol::get_raiz(){
	//retorna la raiz del arbol
	return this->raiz;
}

void Arbol::reestructura_izq(Nodo *&raiz, bool &bul){
	//comienza la reestructuracion izquierda, se crean dos nodos temporales
	Nodo *nodo_a, *nodo_b;
	if(bul== true){
		//si bul es verdadero el arbol crece por un lado
		//se abre un switch dependiente del "label" de la raiz
		switch (stoi(raiz->label)){
			//si label es -1, se re-equilibra el arbol, por lo que se cambia a 0
			case -1: raiz->label = "0";
					break;
			//si label es 0, el arbol no requiere modificacion, label se vuelve 1 y bul se vuelve falso
			case 0: raiz->label = "1";
					bul = false;
					break;
			//si label es 1 se debe reestructurar el arbol para regresar al equilibrio
			case 1://reestructuracion del arbol
					nodo_a = raiz->der;
					//primero se iguala el primer nodo temporal al nodo a la derecha de la raiz
					//y se evalua el label del nodo temporal
					if (stoi(nodo_a->label) >= 0){
						//si label es mayor o igual a 0, comienza rotacion DD
						//el nodo a la derecha de raiz se iguala al nodo que se encuentra a la izqierda del nodo temporal
						raiz->der = nodo_a->izq;
						//el nodo a la izquierda del temporal se iguala a la raiz
						nodo_a->izq = raiz;
						//luego se ingresa a un switch dependiente del label del nodo temporal
						switch(stoi(nodo_a->label)){
							//en el caso 0, el label de raiz se iguala a 1 y el del nodo temporal se iguala a -1
							case 0: raiz->label = "1";
									nodo_a->label = "-1";
									//ya que esto equilibra el arbol, bul se iguala a falso
									bul = false;
									break;
							//en el caso 1, los label de raiz y del nodo temporal se igualan a 0
							case 1: raiz->label = "0";
									nodo_a->label = "0";
									break;
						}
						//finalmente se iguala la raiz al nodo temporal
						raiz = nodo_a;
						//y acaba rotacion DD
					}else{
						//si label es menor a 0, comienza rotacion DI
						//un segundo nodo temporal se iguala al nodo izquierdo del primer nodo temporal
						nodo_b = nodo_a->izq;
						//el nodo a la derecha de raiz se iguala al nodo izquierdo del segundo temporal
						raiz->der = nodo_b->izq;
						//el nodo izquierdo del segundo temporal se iguala a raiz
						nodo_b->izq = raiz;
						//el nodo izquierdo del primer temporal se iguala al derecho del segundo temporal
						nodo_a->izq = nodo_b->der;
						//el nodo derecho del segundo temporal se iguala al primer temporal
						nodo_b->der = nodo_a;
						//luego se evalua el label del segundo nodo temporal
						if(stoi(nodo_b->label) == 1){
							//si label es igual a 1, el label de raiz se reemplaza por -1
							raiz->label = "-1";
						}else{
							//si no lo es, el label de raiz se iguala a 0
							raiz->label = "0";
						}
						//se evalua el label del segundo temporal nuevamente
						if(stoi(nodo_b->label) == -1){
							//si label es igual a -1, el label del primer temporal se reemplaza por 1
							nodo_a->label = "1";
						}else{
							//si no lo es, el label del segundo temporal se iguala a 0
							nodo_a->label = "0";
						}
						//se iguala raiz al segundo temporal, y se modifica el label del segundo temporal a 0
						raiz = nodo_b;
						nodo_b->label = "0";
						//y acaba rotacion DI
					}break;
		}
	}
}
						




void Arbol::reestructura_der(Nodo *&raiz, bool &bul){
	//comienza la reestructuracion derecha, se crean dos nodos temporales
	Nodo *nodo_a, *nodo_b;
	if(bul== true){
		//si bul es verdadero el arbol crece por un lado
		//se abre un switch dependiente del "label" de la raiz
		switch (stoi(raiz->label)){
			//si label es 1, se re-equilibra el arbol, por lo que se cambia a 0
			case 1: raiz->label = "0";
					break;
			//si label es 0, el arbol no requiere modificacion, label se vuelve -1 y bul se vuelve falso
			case 0: raiz->label = "-1";
					bul = false;
					break;
			//si label es -1 se debe reestructurar el arbol para regresar al equilibrio
			case -1: //reestructuracion del arbol
					nodo_a = raiz->izq;
					//primero se iguala el primer nodo temporal al nodo a la izquierda de la raiz
					//y se evalua el label del nodo temporal
					if(stoi(nodo_a->label) <= 0){
						//si label es menor o igual que 0, se hace rotacion II
						raiz->izq = nodo_a->der;
						//el nodo a la izquierda de la raiz se iguala al nodo derecho del nodo temporal
						nodo_a->der = raiz;
						//el nodo derecho del nodo temporal se iguala a la raiz
						//luego se ingresa a un switch dependiente del label del nodo temporal
						switch(stoi(nodo_a->label)){
							//en el caso 0, el label de raiz se iguala a -1 y el del nodo temporal se iguala a 1
							case 0: raiz->label = "-1";
									nodo_a->label = "1";
									//ya que esto equilibra el arbol, bul se iguala a falso
									bul = false;
									break;
							//en el caso -1, los label de raiz y del nodo temporal se igualan a 0
							case -1: raiz->label = "0";
									nodo_a->label = "0";
									break;
						}
						//finalmente se iguala la raiz al nodo temporal
						raiz = nodo_a;
						//y finaliza la rotacion II
					}else{
						//si label es mayor a 0, comienza rotacion ID
						//el segundo nodo temporal se iguala al nodod erecho del primer temporal
						nodo_b = nodo_a->der;
						//el nodo izquierdo de la raiz se iguala al nodo derecho del segundo temporal
						raiz->izq = nodo_b->der;
						//el nodo derecho del segundo temporal se iguala a la raiz
						nodo_b->der = raiz;
						//el nodo derecho del primer temporal se iguala al nodo izquierdod el segundo temporal
						nodo_a->der = nodo_b->izq;
						//el nodo izqueirdo del segundo temporal se iguala al primer temporal
						nodo_b->izq = nodo_a;
						//luego se evalua el label del segundo nodo temporal
						if(stoi(nodo_b->label) == -1){
							//si label es igual a -1, se reemplaza por 1
							raiz->label = "1";
						}else{
							//si no lo es, el label de raiz se iguala a 0
							raiz->label = "0";
						}
						//se evalua el label del segundo temporal nuevamente
						if(stoi(nodo_b->label) == 1){
							//si label es igual a 1, el label del priemr temporal se iguala a -1
							nodo_a->label = "-1";
						}else{
							//si no lo es, el label del segundo temporal se iguala a 0
							nodo_a->label = "0";
						}
						//se iguala raiz al segundo temporal, y se modifica el label del segundo temporal a 0
						raiz = nodo_b;
						nodo_b->label = "0";
						//y termina rotacion ID
					} break;
		}
	}
}



void Arbol::insertar_balanceo (int num, Nodo *&raiz, bool &bul) {
	
    Nodo *nodo = new Nodo, *nodo_a, *nodo_b;
	Nodo *temp = crear_nodo(num);
	
	if(raiz != NULL){
	//si es menor al primero se inserta a su izquierda
		if(num < raiz->numero){	
			//siempre se envia a la llamada recursiva
			insertar_balanceo(num, raiz->izq, bul);
			//el valor por default de bul == false
			if(raiz->izq == NULL){
				//si no existen mas nodos hacia la izquierda, el nodo nuevo se inserta a la izquierda de raiz
				raiz->izq = temp;
				//ya que el arbol crece bul se iguala  true
				bul = true;
			}			
			if(bul == true){
				//si bul es verdadero se modifica el arbol, entrando en un switch dependiente del label de raiz
				switch(stoi(raiz->label)){
					case 1: raiz->label = "0";
							bul = false;
							break;
					case 0: raiz->label = "-1";
							break;
					//si label es -1, se reestructura el arbol
					case -1: nodo_a = raiz->izq;
							if(stoi(nodo_a->label) <= 0){
								//rotacion II
								raiz->izq = nodo_a->der;
								nodo_a->der = raiz;
								raiz->label = "0";			
								raiz = nodo_a;
								//fin rotacion II 
							}else{ 
								//rotacion ID
								nodo_b = nodo_a->der;
								raiz->izq = nodo_b->der;
								nodo_b->der = raiz;
								nodo_a->der = nodo_b->izq;
								nodo_b->izq = nodo_a;
								if(stoi(nodo_b->label) == -1){
									raiz->label = "1";
								}else{
									raiz->label = "0";
								}
								if(stoi(nodo_b->label) == 1){
									nodo_a->label = "-1";
								}else{
									nodo_a->label = "0";
								}
								raiz = nodo_b;
							}
							//acaba rotacion ID
							raiz->label = "0";
							bul = false;
							
							break;
				}
			}
		}else{
			if(num > raiz->numero){
				//si es mayor se ingresa a la derecha
				insertar_balanceo(num, raiz->der, bul);
				//llamada recursiva
				if(raiz->der == NULL){
					//si ya no hay mas nodos hacia la derecha, el nuevo se ingresa a la derecha de raiz
					raiz->der = temp;
					//ya que el arbol crece bul se iguala  true
					bul = true;
				}
				if (bul == true){
					//si bul es verdadero se modifica el arbol, entrando en un switch dependiente del label de raiz
					switch(stoi(raiz->label)){
						case -1: raiz->label = "0";
								bul = false;
								break;
						case 0: raiz->label = "1";
								break;
						//si label es igual a 1, se reestructura el arbol
						case 1: nodo_a = raiz->der;
								if (stoi(nodo_a->label) >= 0){
									//rotacion DD
									raiz->der = nodo_a->izq;
									nodo_a->izq = raiz;
									raiz->label = "0";
									raiz = nodo_a;
									//acaba rotacion DD
								}else{
									//rotacion DI
									nodo_b = nodo_a->izq;
									raiz->der = nodo_b->izq;
									nodo_b->izq = raiz;
									nodo_a->izq = nodo_b->der;
									nodo_b->der = nodo_a;
									if(stoi(nodo_b->label) == 1){
										raiz->label = "-1";
									}else{
										raiz->label = "0";
									}
									if(stoi(nodo_b->label) == -1){
										nodo_a->label = "1";
									}else{
										nodo_a->label = "0";
									}
									raiz = nodo_b;
									//acaba rotacion DI
								}
								
								raiz->label = "0";
								bul = false;
								
								break;
					}
				}
			}else{
				if(num == raiz->numero){
					//si el numero es igual a la informacion dentro de un nodo existente, se muestra un mensaje de error
					cout<<"El numero ingresado ya se encuentra en el arbol"<<endl;
				}		
			}
		}
	}
}
								
	
	
void Arbol::modifica_balanceo(int num, int reemplazo, Nodo *&raiz, bool &bul){
	
	if(raiz != NULL){
		//se busca la informacion en el arbol
		if(num < raiz->numero){
			//si el ingreso es menor al valor dentro de raiz
			//se comprueba que exista una continuacion izquierda
			if(raiz->izq == NULL){
				cout<<"El numero que busca reemplazar no se encuentra en el arbol"<<endl;
			}else{
				//luego se comprueba que el numero de reemplazo no se encuentre en el arbol
				if(reemplazo == raiz->numero){
					cout<<"El numero que ingresó como reemplazo ya se encuentra en el arbol"<<endl;
				}else{
					//si no se encuentra se envia a una llamada recursiva
					modifica_balanceo(num, reemplazo, raiz->izq, bul);
					reestructura_izq(raiz, bul);
					reestructura_der(raiz, bul);
				}
			}
		}else{
			if(num > raiz->numero){
				//si el ingreso es mayor al valor de la raiz
				//se comprueba que exista una continuacion derecha
				if(raiz->der == NULL){
					cout<<"El numero que busca reemplazar no se encuentra en el arbol"<<endl;
				}else{
					if(reemplazo == raiz->numero){
						//luego se comprueba que el numero de reemplazo no se encuentre en el arbol
						cout<<"El numero que ingresó como reemplazo ya se encuentra en el arbol"<<endl;
					}else{
						//si no se encuentra se envia a una llamada recursiva
						modifica_balanceo(num_info, reemplazo, raiz->der, bul);
						reestructura_der(raiz, bul);
						reestructura_izq(raiz, bul);
					}
				}
			}else{
				//si el valor se ubica dentro del arbol comienza la modificacion
				//se comprueba que el reemplazo pueda ser insertado en dicha posicion sin desequilibrar el arbol
				//si se cumplen las condiciones, se reemplaza la informacion en raiz, por reemplazo
				if(raiz->der != NULL && raiz->izq != NULL){
					if(reemplazo > raiz->der->numero || reemplazo < raiz->izq->numero){
						 cout<<"El numero "<<reemplazo<<" no puede ser puesto en esta posicion"<<endl;
					 }else{
						raiz->numero = reemplazo;
						raiz->info = to_string(reemplazo);				
					}
				 }else{
					 if(raiz->izq != NULL && raiz->der == NULL){
						 if(reemplazo < raiz->izq->numero){
							 cout<<"El numero "<<reemplazo<<" no puede ser puesto en esta posicion"<<endl;
						 }else{
							raiz->numero = reemplazo;
							raiz->info = to_string(reemplazo);				
						}
					 }else{
						 if(raiz->der != NULL && raiz->izq == NULL){
							 if(reemplazo > raiz->der->numero){
								 cout<<"El numero "<<reemplazo<<" no puede ser puesto en esta posicion"<<endl;
							 }else{
								raiz->numero = reemplazo;
								raiz->info = to_string(reemplazo);				
							}
						}
					}		
				}	
			}
		}
	}
}	



void Arbol::eliminar_balanceo(int num, Nodo *&raiz, bool &bul){
    Nodo *nodo = new Nodo;
    Nodo *temp = new Nodo;
    Nodo *temp1 = new Nodo;
    bool comp;
    
    
	if(raiz != NULL){
		if(num < raiz->numero){
			//si es menos se lanza una llamada recursiva por la izquierda
			eliminar_balanceo(num, raiz->izq, bul);
			reestructura_izq(raiz, bul);
		}else{
			if(num > raiz->numero){
				//si es mas, la llamada se lanza por la derecha
				eliminar_balanceo(num, raiz->der, bul);
				reestructura_der(raiz, bul);
			}else{
				nodo = raiz;
				bul = true;
				if(nodo->der == NULL){
					//si la coincidencia no tiene descendencia hacia la derecha
					//se lo reemplaza por su descendencia izquierda
					raiz = nodo->izq;
				}else{
					if(nodo->izq == NULL){
						//si tiene descendencia por la derecha pero no por la izquierda
						//se lo reemplaza por la descendencia derecha
						raiz = nodo->der;
					}else{
						//si tiene descendencia por amnos lados
						//se crea un temporal que guarda la descendencia izquierda
						temp = raiz->izq;
						//bul se iguala a falso
						bul = false;
						//se ingresa a un while que devuelve al nodo mas a la derecha de temp
						while(temp->der != NULL){
							temp1 = temp;
							temp = temp->der;
							bul = true;
						}
						//la informacion dentro de raiz se reemplaza por la informacion de temp
						raiz->numero = temp->numero;
						raiz->info = temp->info;
						//nodo se iguala a temp
						nodo = temp;
						//se evalua bul
						if(bul == true){
							//si bul es verdadero, el nodo a la derecha de temp1 se reemplaza por el nodo a la derecha de temp
							temp1->der = temp->izq;
						}else{
							//si bul es falso el nodo a la izquierda de raiz, se iaguala al nodo de la izquierda de temp
							raiz->izq = temp->izq;
						}
						//se llama la reestructuracion derecha
						reestructura_der(raiz->izq, bul);
					}
				}
				//se elimina la informacion de nodo
				delete nodo;
				nodo = nullptr;
			}							
		}						
	}else{
		//si no existe coincidencia se muestra un mensaje de error
		cout<<"El numero que intenta eliminar, no se encuentra"<<endl;
		cout<<"intente nuevamente"<<endl;
	}
}

/* ofstream es el tipo de dato correspondiente a archivos en cpp (el llamado es ofstream &nombre_archivo). */
int Arbol::recorrerArbol(Nodo *p, ofstream &archivo, int cont) {
	
  string infoTmp;
  /* Se enlazan los nodos del grafo, para diferencia entre izq y der a cada nodo se le entrega un identificador al final, siendo i: izquierda
   * y d: derecha, esto se cumplirá para los casos en donde los nodos no apunten a ningún otro (nodos finales) 
   * */
  if (p != NULL) {
    infoTmp = p->info; 
      archivo << infoTmp << " [style=filled fillcolor=magenta];" << endl;
	/* Por cada nodo ya sea por izq o der se escribe dentro de la instancia del archivo */  
    if (p->izq != NULL) {
      archivo<< p->info << "->" << p->izq->info << " [label="<< p->izq->label<<"];" << endl;
      archivo << p->izq->info << " [style=filled fillcolor=magenta];" << endl;
      
    } else {
	  infoTmp = p->info;
	  archivo <<"null" << cont << " [shape=point]"  << endl;
      archivo << p->info << "->" << "null" << cont<< endl;
		cont = cont+1;
    }
    
    if (p->der != NULL) {
      archivo << p->info << "->" << p->der->info << " [label="<< p->der->label<<"];" << endl;
      archivo << p->der->info << " [style=filled fillcolor=magenta];" << endl;
    
    } else {
	  infoTmp = p->info;
	  archivo <<"null" << cont << " [shape=point]"  << endl;
      archivo << p->info << "->" << "null" << cont   << endl;
		cont = cont+1;
    }
   
    /* Se realizan los llamados tanto por la izquierda como por la derecha para la creación del grafo */
    cont = recorrerArbol(p->izq, archivo, cont);
    cont = recorrerArbol(p->der, archivo, cont); 

  }
  return cont;
}



void Arbol::crearGrafo(Nodo *raiz) {
    ofstream archivo;  
    int cont =0;
    /* Se abre/crea el archivo datos.txt, a partir de este se generará el grafo */ 
    archivo.open("grafo.txt");
    /* Se escribe dentro del archivo datos.txt "digraph G { " */ 
    archivo << "digraph G {" << endl;
    
    archivo << "null [shape=point];" << endl;
    archivo << "null->"<<raiz->info<< " [label="<<raiz->label<<"]"<<endl;
    /* Llamado a la función recursiva que genera el archivo de texto para creación del grafo */    
    cont = recorrerArbol(raiz, archivo, cont);
    /* Se termina de escribir dentro del archivo datos.txt*/
    archivo << "}" << endl;
    archivo.close();
    
    /* genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
    system("eog grafo.png");
}
