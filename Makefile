prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Ejer_1.cpp Arbol.cpp
OBJ = Ejer_1.o Arbol.o
APP = ejer_1

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)

